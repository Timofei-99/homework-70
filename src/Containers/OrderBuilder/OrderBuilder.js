import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "../../store/actions/productsAction";
import Products from "../../Components/Products/Products";
import {addProducts} from "../../store/actions/cartAction";

const OrderBuilder = () => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.products.products);

    useEffect(() => {
        dispatch(fetchProducts())
    }, [dispatch]);

    const addElement = (name) => {
        dispatch(addProducts(name));
    };


    return (
        <div style={{width:"45%"}}>
            <h2>Menu</h2>
            {products.map(product => (
                <Products
                    key={product.name}
                    img={product.img}
                    title={product.name}
                    price={product.price}
                    add={addElement}
                    id={product.product}
                />
            ))}
        </div>
    );
};

export default OrderBuilder;