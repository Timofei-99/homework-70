import React from 'react';
import './Cart.css';
import {useDispatch, useSelector} from "react-redux";
import Orders from "../../Components/Orders/Orders";
import {openModal, removeProducts} from "../../store/actions/cartAction";
import Modal from "../../Components/UI/Modal/Modal";

const Cart = () => {
    const dispatch = useDispatch();
    const total = useSelector(state => state.orders.total);
    const cart = useSelector(state => state.orders.cart);
    const modal = useSelector(state => state.orders.modal);

    const remove = (name) => {
        dispatch(removeProducts(name));
    };

    const open = () => {
        dispatch(openModal())
    };


    return (
        <>
            <div>
                <h2>Cart</h2>
                <div className='cart'>
                    <ul>
                        {Object.keys(cart).map(item => {
                            return cart[item] > 0 ? <Orders
                                key={item}
                                name={item}
                                count={cart[item]}
                                remove={remove}
                            /> : null
                        })}
                    </ul>
                    <hr/>
                    <ul className='delivery'>
                        <li>Доставка: 150</li>
                        <li>Итого: {total}</li>
                        {total !== 0 ? <li>
                                <button onClick={open}>Отправить заказ</button>
                            </li>
                            : null}
                    </ul>
                    <Modal
                        show={modal}
                    />
                </div>
            </div>
        </>
    );
};

export default Cart;