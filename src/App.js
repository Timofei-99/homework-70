import OrderBuilder from "./Containers/OrderBuilder/OrderBuilder";
import Cart from "./Containers/Cart/Cart";

function App() {
    return (
        <div className="App" style={{display:"flex", justifyContent: 'space-around'}}>
            <OrderBuilder/>
            <Cart/>
        </div>
    );
}

export default App;
