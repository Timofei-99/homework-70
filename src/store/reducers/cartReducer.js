import {
    ADD_PRODUCTS,
    CLOSE_MODAL, INIT_ORDERS,
    OPEN_MODAL,
    REMOVE_PRODUCTS, SUBMIT_ORDER_FAILURE,
    SUBMIT_ORDER_REQUEST,
    SUBMIT_ORDER_SUCCESS
} from "../actions/cartAction";
import {PRICES} from "../../constans";

const initialState = {
    cart: {
        bread: 0,
        plov: 0,
        shakarap: 0,
    },
    total: 0,
    modal: false,
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case INIT_ORDERS:
            return {...initialState}
        case ADD_PRODUCTS:
            return {
                ...state,
                cart: {
                    ...state.cart,
                    [action.payload]: state.cart[action.payload] + 1
                },
                total: state.total + PRICES[action.payload],
            }
        case  REMOVE_PRODUCTS:
            return {
                ...state,
                cart: {
                    ...state.cart,
                    [action.payload]: state.cart[action.payload] - 1
                },
                total: state.total - PRICES[action.payload],
            }
        case OPEN_MODAL:
            return {...state, modal: true}
        case CLOSE_MODAL:
            return {...state, modal: false}
        case SUBMIT_ORDER_REQUEST:
            return {...state}
        case SUBMIT_ORDER_SUCCESS:
            return {...state, modal: false}
        case SUBMIT_ORDER_FAILURE:
            return {...state}
        default:
            return state;
    }
};

export default cartReducer;