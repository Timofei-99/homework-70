import {FETCH_PRODUCTS_FAILURE, FETCH_PRODUCTS_REQUEST, FETCH_PRODUCTS_SUCCESS} from "../actions/productsAction";

const initialState = {
    products: [],
    error: ''
};

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_REQUEST:
            return {...state}
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.payload}
        case FETCH_PRODUCTS_FAILURE:
            return {...state, error: action.payload}
        default:
            return state;
    }
}

export default productReducer;