import axiosAPI from "../../axiosAPI";

export const FETCH_PRODUCTS_REQUEST = 'FETCH_PRODUCTS_REQUEST';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';

export const fetchProductsRequest = () => ({type: FETCH_PRODUCTS_REQUEST});
export const fetchProductsSuccess = (products) => ({type: FETCH_PRODUCTS_SUCCESS, payload: products});
export const fetchProductsFailure = (error) => ({type: FETCH_PRODUCTS_FAILURE, payload: error});

export const fetchProducts = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchProductsRequest());
            const response = await axiosAPI.get('products.json');
            const products = Object.keys(response.data).map(product => ({
                ...response.data[product],
                product
            }))
            dispatch(fetchProductsSuccess(products));
        } catch (error) {
            dispatch(fetchProductsFailure(error));
        }
    }
}