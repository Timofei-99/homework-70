import axiosAPI from "../../axiosAPI";

export const ADD_PRODUCTS = 'ADD_PRODUCTS';
export const REMOVE_PRODUCTS = 'REMOVE_PRODUCTS';
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const SUBMIT_ORDER_REQUEST = 'SUBMIT_ORDER_REQUEST';
export const SUBMIT_ORDER_SUCCESS = 'SUBMIT_ORDER_SUCCESS';
export const SUBMIT_ORDER_FAILURE = 'SUBMIT_ORDER_FAILURE';
export const INIT_ORDERS = 'INIT_ORDERS';

export const addProducts = (name) => ({type: ADD_PRODUCTS, payload: name});
export const removeProducts = (name) => ({type: REMOVE_PRODUCTS, payload: name});
export const openModal = () => ({type: OPEN_MODAL});
export const closeModal = () => ({type: CLOSE_MODAL});
export const submitOrderRequest = () => ({type: SUBMIT_ORDER_REQUEST});
export const submitOrderSuccess = value => ({type: SUBMIT_ORDER_SUCCESS});
export const submitOrderFailure = () => ({type: SUBMIT_ORDER_FAILURE});
export const initOrders = () => ({type: INIT_ORDERS});

export const postOrders = (value) => {
    return async (dispatch) => {
        try {
            dispatch(submitOrderRequest());
            await axiosAPI.post('orders.json', value);
            dispatch(submitOrderSuccess());
        } catch (error) {
            dispatch(submitOrderFailure());
        }
    }
};



