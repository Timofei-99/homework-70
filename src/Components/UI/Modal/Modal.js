import React, {useState} from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";
import {useDispatch, useSelector} from "react-redux";
import {closeModal, initOrders, postOrders} from "../../../store/actions/cartAction";

const Modal = props => {
    const [user, setUser] = useState({
        name: '',
        address: '',
        tel: ''
    });
    const dispatch = useDispatch();
    const cart = useSelector(state => state.orders.cart);

    const onSubmit = (e) => {
        e.preventDefault();
        dispatch(postOrders({cart, user}))
        dispatch(initOrders())
        setUser({
            name: '',
            address: '',
            tel: ''
        });
    };

    const change = (e) => {
        const {name, value} = e.target
        setUser((prev) => ({...prev, [name]: value}));
    }

    const close = () => {
        dispatch(closeModal());
    }

    return (
        <>
            <Backdrop
                show={props.show}
                onClick={props.close}
            />
            <div
                className="Modal"
                style={{
                    transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                    opacity: props.show ? '1' : '0',
                }}
            >
                <div className='inputs'>
                    <h2>Оформить заказ</h2>
                    <form onSubmit={onSubmit}>
                        <div>
                            <input className='inpt' name={"name"} value={user.name} placeholder='Name' type="text" onChange={change}/>
                        </div>
                        <div>
                            <input className='inpt' name={"address"} value={user.address} placeholder='Address' type="text" onChange={change}/>
                        </div>
                        <div>
                            <input className='inpt' name={"tel"} value={user.tel} placeholder='Tel' type="text" onChange={change}/>
                        </div>
                        <button className='submitBtn' type={"submit"}>Отправить заказ</button>
                        <button onClick={close} className='closeBtn' type={"button"}>Закрыть</button>
                    </form>
                </div>

            </div>
        </>
    );
};

export default Modal;