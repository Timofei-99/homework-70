import React from 'react';
import './Orders.css';

const Orders = ({name, count, remove}) => {
    return (
        <li className='list'>
            <span>{name}</span>
            <span> x{count}</span>
            <button className='del' onClick={()=>remove(name)}>X</button>
        </li>
    );
};

export default Orders;