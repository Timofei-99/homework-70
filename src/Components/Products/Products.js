import React from 'react';
import './Products.css';

const Products = ({img, title, price, add, id}) => {
    return (
        <div className='card'>
            <img src={img} alt=""/>
            <div className="content">
                <h3>{title}</h3>
                <span>{price}</span>
            </div>
            <button className='addBtn' onClick={() => add(id)}>Add to cart</button>
        </div>
    );
};

export default Products;